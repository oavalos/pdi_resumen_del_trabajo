# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 15:39:53 2020

@author: osvad
"""


from numba import jit 
import imageio
import numpy as np
import sys
from skimage.util import img_as_ubyte

@jit
def convertir_YIQ(im):

    imagen=imageio.imread(im)/256
    

    mat_yiq = np.array([[0.299 ,0.595716,0.211456],
                    [0.587 , -0.274453 , -0.522591],
                    [0.114 , -0.321263 , 0.311135]])

    yiq = np.matmul(imagen.reshape((-1,3)),mat_yiq).reshape(imagen.shape)
   
 
    
    row,column=yiq[:,:,0].shape

    for i in range(row):
        for j in range (column):
            if yiq[:,:,0][i,j]>1:
                yiq[:,:,0][i,j]=0.999999


    for i in range(row):
        for j in range (column):
            if yiq[:,:,1][i,j]>0.5957:
                yiq[:,:,1][i,j]=0.5957
            if yiq[:,:,1][i,j]<-0.5957:
                yiq[:,:,1][i,j]=-0.5957
            
    for i in range(row):
        for j in range (column):
            if yiq[:,:,2][i,j]>0.5226:
                yiq[:,:,2][i,j]=0.5226
            if yiq[:,:,2][i,j]<-0.5226:
                yiq[:,:,2][i,j]=-0.5226
        
        


    #rgb = np.matmul(yiq.reshape((-1,3)),np.linalg.inv(mat_yiq)).reshape(yiq.shape)


    return yiq

    

@jit
def operacion_IMAG(im1,im2,operacion,formato):
    
    """Suma y resta dos imagenes de igual resolucion para ambas operacion 
    suma y resta y en funcion del formato que se desee ya sea clampeado oh 
    promediado"""
    
    imagen1=convertir_YIQ(im1)
    imagen2=convertir_YIQ(im2)
    
    row,column,canales=imagen1.shape
    
    YIQ_clampeada = np.zeros((row, column, 3))
    YIQ_PROMEDIO = np.zeros((row, column, 3))
    
    Y1=imagen1[:,:,0]
    Y2=imagen2[:,:,0]
    
    I1=imagen1[:,:,1]
    I2=imagen2[:,:,1]
    
    Q1=imagen1[:,:,2]
    Q2=imagen2[:,:,2]
    
    if operacion=="suma" and formato=="YIQ_clampeada":
        
        YIQ_clampeada[:,:,0]=Y1 +Y2
        
        for i in range(row):
            
            for j in range (column):
                
                if YIQ_clampeada[:,:,0][i,j]>1:
                    
                    YIQ_clampeada[:,:,0][i,j]=1
                    
        YIQ_clampeada[:,:,1]=(Y1*I1 + Y2*I2)/(Y1+Y2)
        YIQ_clampeada[:,:,2]=(Y1*Q1 + Y2*Q2)/(Y1+Y2)
        
        resultado=YIQ_clampeada
        
    elif formato=="if_ligther" and operacion=="suma":
        
        for i in range(row):
            
            for j in range (column):
                if Y1[i,j]>Y2[i,j]:
                    
                    YIQ_clampeada[:,:,0][i,j]=Y1[i,j]
                    YIQ_clampeada[:,:,1][i,j]=I1[i,j]
                    YIQ_clampeada[:,:,2][i,j]=Q1[i,j]
                else:
                    YIQ_clampeada[:,:,0][i,j]=Y2[i,j]
                    YIQ_clampeada[:,:,1][i,j]=I2[i,j]
                    YIQ_clampeada[:,:,2][i,j]=Q2[i,j]
                    
        resultado=YIQ_clampeada
        
    elif operacion=="suma" and formato=="YIQ_PROMEDIO":
        
        YIQ_PROMEDIO[:,:,0]=(Y1 +Y2)/2
      
        YIQ_PROMEDIO[:,:,1]=(Y1*I1 + Y2*I2)/(Y1+Y2)
        YIQ_PROMEDIO[:,:,2]=(Y1*Q1 + Y2*Q2)/(Y1+Y2)
        
        resultado=YIQ_PROMEDIO
        
    elif operacion =="resta" and formato=="YIQ_clampeada":
        
        YIQ_clampeada[:,:,0]=Y1- Y2
        
        for i in range(row):
            for j in range (column):
                if YIQ_clampeada[:,:,0][i,j]<0:
                    YIQ_clampeada[:,:,0][i,j]=0
                    
        YIQ_clampeada[:,:,1]=(Y1*I1 - Y2*I2)/(Y1+Y2)
        YIQ_clampeada[:,:,2]=(Y1*Q1 - Y2*Q2)/(Y1+Y2)
        
        resultado=YIQ_clampeada
        
    elif formato=="if_dark" and operacion=="resta":
        for i in range(row):
            for j in range (column):
                if imagen1[:,:,0][i,j]>imagen2[:,:,0][i,j]:
                    
                    YIQ_clampeada[:,:,0][i,j]=Y2[i,j]
                    YIQ_clampeada[:,:,1][i,j]=I2[i,j]
                    YIQ_clampeada[:,:,2][i,j]=Q2[i,j]
                else:
                    YIQ_clampeada[:,:,0][i,j]=Y1[i,j]
                    YIQ_clampeada[:,:,1][i,j]=I1[i,j]
                    YIQ_clampeada[:,:,2][i,j]=Q1[i,j]
        resultado=YIQ_clampeada
        
    elif operacion=="resta" and formato=="YIQ_PROMEDIO":
        
        YIQ_PROMEDIO[:,:,0]=(imagen1[:,:,0] -imagen2[:,:,0])/2
      
        YIQ_PROMEDIO[:,:,1]=(Y1*I1 - Y2*I2)/(Y1+Y2)
        YIQ_PROMEDIO[:,:,2]=(Y1*Q1 - Y2*Q2)/(Y1+Y2)
        resultado=YIQ_PROMEDIO 
        
    return resultado

def procesar(im1,im2,operacion,formato,path_save):
    
    """Funcion principal para realizar el procesado de la imagenes en funcion 
    de estos de las operaciones deseadas ya sea suma oh resta """
    
    matrix=operacion_IMAG(im1, im2, operacion, formato)
    im_uint8=img_as_ubyte(matrix)#para convertir la imagen a de float64 a uint8
    imageio.imwrite(path_save,im_uint8) 
    
if __name__ == '__main__':
    if len(sys.argv) > 5:
        im1=(sys.argv[1])
        im2=(sys.argv[2])
        operacion=(sys.argv[3])
        formato=(sys.argv[4])
        path_save=(sys.argv[5])
    procesar(im1,im2,operacion,formato,path_save)

    
    
                
                
                
        