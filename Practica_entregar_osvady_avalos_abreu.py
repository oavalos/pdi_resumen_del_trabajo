# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 12:02:19 2020

@author: osvad
"""

from numba import jit 
import imageio
import numpy as np
from skimage.util import img_as_ubyte
import sys 

@jit
def manipular_luminacia_saturacion(im,alpha,beta,path_save_yiq,path_save_rgb,path_save_join):
    """im= direccion de la imagen  cargar
    path_save_yiq=direccion donde salvar la imagen
    path_save_rgb=direccion donde salvar la imagen rgb
    path_save_join=camino para salvar la imagen """
    
    imagen=imageio.imread(im)/256

    mat_yiq = np.array([[0.299 ,0.595716,0.211456],
                    [0.587 , -0.274453 , -0.522591],
                    [0.114 , -0.321263 , 0.311135]])

    yiq = np.matmul(imagen.reshape((-1,3)),mat_yiq).reshape(imagen.shape)
    alpha_ =float(alpha)
    beta_ = float(beta)
    yiq[:,:,0] *= alpha_; yiq[:,:,1] *= beta_; yiq[:,:,2] *= beta_
    # yiq[:,:,0]=alpha_*yiq[:,:,0]
    # yiq[:,:,1]=beta_*yiq[:,:,1]
    # yiq[:,:,2]=beta_*yiq[:,:,2]
    row,column,channel=yiq.shape

    for i in range(row):
        for j in range (column):
            if yiq[:,:,0][i,j]>1:
                yiq[:,:,0][i,j]=0.999999


    for i in range(row):
        for j in range (column):
            if yiq[:,:,1][i,j]>0.5957:
                yiq[:,:,1][i,j]=0.5957
            if yiq[:,:,1][i,j]<-0.5957:
                yiq[:,:,1][i,j]=-0.5957
            
    for i in range(row):
        for j in range (column):
            if yiq[:,:,2][i,j]>0.5226:
                yiq[:,:,2][i,j]=0.5226
            if yiq[:,:,2][i,j]<-0.5226:
                yiq[:,:,2][i,j]=-0.5226
        
    im_uint8=img_as_ubyte(yiq)#para convertir la imagen a de float64 a uint8
       
    imageio.imsave(path_save_yiq,im_uint8)
    
    rgb = np.matmul(yiq.reshape((-1,3)),np.linalg.inv(mat_yiq)).reshape(yiq.shape)
    
    for i in range(row):
       for j in range (column):
           for k in range(channel):
                if rgb[i,j,k]>1:
                    rgb[i,j,k]=0.999
                if rgb[i,j,k]<-1:
                    rgb[i,j,k]=-0.999
                    
    im_uint8_rgb=img_as_ubyte(rgb)#para convertir la imagen a de float64 a uint8

    imageio.imsave(path_save_rgb,im_uint8_rgb)
    
    imageio.imsave(path_save_join,np.hstack((img_as_ubyte(imagen),im_uint8,im_uint8_rgb)))
    
    return 

if __name__ == '__main__':
    if len(sys.argv) > 6:
        im =(sys.argv[1])
        alpha=(sys.argv[2])
        beta=(sys.argv[3])
        path_save_yiq=(sys.argv[4])
        path_save_rgb=(sys.argv[5])
        path_save_join=(sys.argv[6])
       
    manipular_luminacia_saturacion(im,alpha,beta,path_save_yiq,path_save_rgb,path_save_join)


manipular_luminacia_saturacion('/Users/osvad/Dropbox/PDI/african-lion-951778_1920.jpg', 1
                              , 1.5
                               , "/Users/osvad/Dropbox/PDI/imagen_nueva.jpg"
                               , "/Users/osvad/Dropbox/PDI/imagen_nueva_RGB.jpg"
                              ,"/Users/osvad/Dropbox/PDI/uni.jpg")


#manipular_luminacia_saturacion('/home/osvady/Dropbox/PDI/african-lion-951778_1920.jpg', 1, 1.5, "/home/osvady/Dropbox/PDI/imagen_nueva.jpg"
#                               , "/home/osvady/Dropbox/PDI/imagen_nueva_RGB.jpg"
#                               ,"/home/osvady/Dropbox/PDI/imagen_nueva_union.jpg")

